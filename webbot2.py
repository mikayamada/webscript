import requests
from bs4 import BeautifulSoup

requisicao = requests.get('https://www.dolarhoje.net.br/dolar-comercial/')
soup = BeautifulSoup(requisicao.content, "html.parser")
valor = soup.select("table tbody tr td")

for i in valor:
    if "Dólar" not in i.text and "Euro" not in i.text:
        print(i.text)