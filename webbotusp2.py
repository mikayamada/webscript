import requests
from lxml import html

Arquivo = open('taxadejuros.csv', 'w')

requisicao = requests.get('http://idg.receita.fazenda.gov.br/orientacao/tributaria/pagamentos-e-parcelamentos/taxa-de-juros-selic')

tree = html.fromstring(requisicao.content)
cont = 2
linha = 2

while linha <= 13:

    taxa = tree.xpath('//*[@id="parent-fieldname-text"]/table[2]/tbody/tr[%d]/td[1]/b/text()' %cont)
    ano = tree.xpath('//*[@id="parent-fieldname-text"]/table[2]/tbody/tr[%d]/td[2]/text()' %cont)
    porc1 = tree.xpath('//*[@id="parent-fieldname-text"]/table[2]/tbody/tr[%d]/td[3]/text()' %cont)
    porc2 = tree.xpath('//*[@id="parent-fieldname-text"]/table[2]/tbody/tr[%d]/td[4]/text()' %cont)
    porc3 = tree.xpath('//*[@id="parent-fieldname-text"]/table[2]/tbody/tr[%d]/td[5]/text()' %cont)
    porc4 = tree.xpath('//*[@id="parent-fieldname-text"]/table[2]/tbody/tr[%d]/td[6]/text()' %cont)
    porc5 = tree.xpath('//*[@id="parent-fieldname-text"]/table[2]/tbody/tr[%d]/td[7]/text()' %cont)
    porc6 = tree.xpath('//*[@id="parent-fieldname-text"]/table[2]/tbody/tr[%d]/td[8]/text()' %cont)
    porc7 = tree.xpath('//*[@id="parent-fieldname-text"]/table[2]/tbody/tr[%d]/td[9]/text()' %cont)

    print ('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}'.format ((taxa), (ano), (porc1), (porc2), (porc3), (porc4), (porc5), (porc6), (porc7)))
    aux = str(taxa).replace ("['", "" ).replace ("']", "") + ' | ' + str(ano).replace ("['", "" ).replace ("']", "") + ' | ' + str(porc1).replace ("['", "" ).replace ("']", "") + ' | ' + str(porc2).replace ("['", "" ).replace ("']", "") + ' | ' + str(porc3).replace ("['", "" ).replace ("']", "") + ' | ' + str(porc4).replace ("['", "" ).replace ("']", "") + ' | ' + str(porc5).replace ("['", "" ).replace ("']", "") + ' | ' + str(porc6).replace ("['", "" ).replace ("']", "") + ' | ' + str(porc7).replace ("['", "" ).replace ("']", "") + '\n'

    Arquivo.write(aux)
    cont = cont + 1
    linha = linha + 1

